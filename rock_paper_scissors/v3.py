# import random module
from random import randint

#welcome message
print("+-------------ROCK, PAPER, SCISSORS-------------+")
print("|                                               |")
print("|  Hello, and welcome to Rock, Paper, Scissors  |")
print("|  - Rage Against the Machine Edition           |")
print("|                                               |")
print("|  Play against a super friendly robot, just    |")
print("|  make sure you're friendly too. You don't     |")
print("|  want any angry robots to seek vengeance for  |")
print("|  your crimes against their kind, in the       |")
print("|  distant future.                              |")
# rules
print("+-------------------THE RULES-------------------+")
print("|                                               |")
print("|  * Rock wins against scissors                 |")
print("|  * Scissors win against paper                 |")
print("|  * Paper wins against rock                    |")
print("|                                               |")
print("+-----------------------------------------------+")

# determine computer's choice
random_num = randint(1, 3)
if random_num == 1:
	computer_choice = "ROCK"
elif random_num == 2:
		computer_choice = "PAPER"
elif random_num == 3:
	computer_choice = "SCISSORS"

# ask for human input
human_choice = input("Human, make your choice: ")

# check if human choice is valid
if not human_choice:
	print("You have to make a choice!")
elif not (human_choice.lower() == "rock" or human_choice.lower() == "paper" or  human_choice.lower() == "scissors"):
	# check if choice is rock, paper, or scissors
	print("Your choice has to either be rock, paper, or scissors.")
else:
	# print computer's choice
	print("The computer's choice was: " + computer_choice)
	# determine winner
	if human_choice.lower() == computer_choice.lower():
		    print("It's a tie!")
	elif human_choice.lower() == "rock" and computer_choice.lower() == "scissors":
	    print("The human wins!")
	elif human_choice.lower() == "paper" and computer_choice.lower() == "rock":
	    print("The human wins!")
	elif human_choice.lower() == "scissors" and computer_choice.lower() == "paper":
	    print("The human wins!")
	else:
	    print("The computer wins!")
	