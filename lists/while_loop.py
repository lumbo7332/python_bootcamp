colors = ["Purple", "Teal", "Magenta", "Crimson", "Emerald"]

index = 0
while index < len(colors):
	print(f"{index}: {colors[index]}") # ex: "0: Purple"
	index += 1